#!/usr/bin/env python3

import re
import math
import datetime
import sys

import certstream
import whois

import db
import time
import os
from tld import get_tld

bad_registrars = ["Key-Systems GmbH"]
certstream_url = 'wss://certstream.calidog.io'

def entropy(string):
    """Calculates the Shannon entropy of a string"""
    prob = [ float(string.count(c)) / len(string) for c in dict.fromkeys(list(string)) ]
    entropy = - sum([ p * math.log(p) / math.log(2.0) for p in prob ])
    return entropy

def new_domain_check(domain):
        try:
            registration_date = whois.whois(domain).creation_date.date()
            today = datetime.date.today()
            difference = today - registration_date

            if difference.days < 7:
                return True
            else:
                return False
        except Exception as Error:
            # Assume that any error here means that the whois information
            # didn't contain any date information and treat it as "old" domain
            # to avoid false positives
            return False

def bad_registrar_check(domain):
    try:
        registrar = whois.whois(domain).registrar
        for bad_registrar in bad_registrars:
            if bad_registrar in registrar:
                return True
        return False
    except Exception as Error:
        # Assume that any error here means that the registrar isn't suspicious / malicious
        return False

def score_domain(domain):
    score = 0


    if new_domain_check(domain):
        score += 10
    if bad_registrar_check(domain):
        score += 10

    # Higher entropy is kind of suspicious
    score += int(round(entropy(domain)*10))

    words_in_domain = re.split("\W+", domain)

    # ie. detect fake .com (ie. *.com-account-management.info)
    if words_in_domain[0] in ['com', 'net', 'org']:
        score += 10

    # Lots of '-' (ie. www.paypal-datacenter.com-acccount-alert.com)
    if 'xn--' not in domain and domain.count('-') >= 4:
        score += domain.count('-') * 3

    # Deeply nested subdomains (ie. www.paypal.com.security.accountupdate.gq)
    if domain.count('.') >= 3:
        score += domain.count('.') * 3

    return score

def callback(message, context):
    db.initialize("./domains.db")

    """Callback handler for certstream events."""
    if message['message_type'] == "heartbeat":
        return

    if message['message_type'] == "certificate_update":
        domain = message['data']['leaf_cert']['subject']['CN']

        # Remove initial '*.' for wildcard certificates bug
        if domain.startswith('*.'):
            domain = domain[2:]

        score = score_domain(domain.lower())
        
        # If issued from a free CA = more suspicious
        if "Let's Encrypt" == message['data']['leaf_cert']['issuer']['O']:
            score += 10
        
        db_data = {}
        db_data["domain"] = domain
        db_data["timestamp"] = time.time()
        
        if score >= 90:
            db_data["classification"] = "definite"
            db.insert("./domains.db", db_data)
        elif score >= 80:
            db_data["classification"] = "likely"
            db.insert("./domains.db", db_data)
        elif score >= 70:
            db_data["classification"] = "potentially"
            db.insert("./domains.db", db_data)

if __name__ == '__main__':
    certstream.listen_for_events(callback, url=certstream_url)
