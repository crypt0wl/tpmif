#!/usr/bin/env python3

import sqlite3
import sys

def initialize(db):
    init_table_query = """
        CREATE TABLE IF NOT EXISTS domains (
            domain TEXT UNIQUE NOT NULL,
            classification TEXT NOT NULL,
            timestamp INTEGER NOT NULL);
        """
    try:
        db_connection = sqlite3.connect(db)
        db_cursor = db_connection.cursor()
        db_cursor.execute(init_table_query)
        db_connection.commit()
        db_connection.close()
        
    except Exception as Error:
        print(f"[!] Could not connect to {db}. Exiting ..")
        sys.exit()

def insert(db, data):
    insert_data_query = "INSERT INTO domains VALUES (?, ?, ?)"
    insert_data_params = (data["domain"], data["classification"], data["timestamp"])

    try:
        db_connection = sqlite3.connect(db)
        db_cursor = db_connection.cursor()
        db_cursor.execute(insert_data_query, insert_data_params)
        db_connection.commit()
        db_connection.close()
        print(f"[*] Successfully inserted {data} ..")

    except Exception as Error:
        print(Error)
