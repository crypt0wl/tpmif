#### tpmif

"The poor man's threat intelligenced feed" is a script that looks (by utilizing
the [Certificate Transparency logs]) at the continuous stream of newly issued
TLS-certificates and tries to guess if the domain behind the certificate is
malicious (by looking at entropy, domain age, registrar, weird combinations of
characters, ..).

If a domain is found to be malicious it's stored in a database (currently only
SQLite is supported), from which they can be exported into various blocklist
formats by an included script. There are plans to eventually turn everything
into a feed of malicious URLs that can be requested via API, but that's not
even remotely implemented yet.

#### Requirements

The required third-party modules are listed in `requirements.txt`:

`pip install -r requirements.txt`

#### Usage

#### Credits

[x0rz](https://github.com/x0rz), with his
[phishing_catcher](https://github.com/x0rz/phishing_catcher) first came up with
a tool to utilize the Certificate Transparency logs for security purposes, I
stole most of the ideas and little tricks from him.
